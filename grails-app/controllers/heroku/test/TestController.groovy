package heroku.test

import grails.converters.JSON

class TestController {

    def index() {
        def users = User.findAll()
        render users as JSON
    }
}
