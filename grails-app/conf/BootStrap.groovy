import heroku.test.User

class BootStrap {

    def init = { servletContext ->
        ['luke','han'].each {
            User user = new User(username: it).save(failOnError: true)
        }
    }
    def destroy = {
    }
}
