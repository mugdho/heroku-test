package heroku.test

class User {
    Long id
    Date dateCreated
    Date lastUpdated
    Long version

    String username

    static mapping = {
        table 'app_user'
        version true
        id generator: 'sequence', column: 'app_user_id', params: [sequence: 'seq_app_user']
    }

    static constraints = {
    }
}
